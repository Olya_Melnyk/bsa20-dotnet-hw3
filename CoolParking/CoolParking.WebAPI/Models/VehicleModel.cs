﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public class VehicleModel
    {
        //[JsonProperty("id")]
        public string Id { get; set; }
        //[JsonProperty("balance")]
        public decimal Balance { get; set; }
        //[JsonProperty(Required = Required.Always)]
        //[Microsoft.AspNetCore.Mvc.ModelBinding.BindNever]
        public int VehicleType { get; set; }
    }
}
