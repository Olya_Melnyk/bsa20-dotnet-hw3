﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI
{
    public class TopUpModel
    {
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public decimal Sum { get; set; }
    }
}
