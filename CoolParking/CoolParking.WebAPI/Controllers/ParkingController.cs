﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet("balance")]
        public IActionResult GetBalance()
        {
            return new JsonResult(_parkingService.GetBalance());
        }
        [HttpGet("capacity")]
        public IActionResult GetCapacity()
        {
            return new JsonResult(_parkingService.GetCapacity());
        }
        [HttpGet("freePlaces")]
        public IActionResult GetFreePlaces()
        {
            return new JsonResult(_parkingService.GetFreePlaces());
        }
    }
}