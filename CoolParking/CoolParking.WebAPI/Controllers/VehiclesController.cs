﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {

        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet()]
        public IActionResult GetVehicles()
        {
            return new JsonResult( _parkingService.GetVehicles());
        }
        [HttpGet("{id}")]
        public IActionResult GetVehicleById(string id)
        {
            if (!Vehicle.Validation(id)) { return BadRequest(); }
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (vehicle == null) { return NotFound(); }
            else { return new JsonResult(vehicle); }
        }
        [HttpPost]
        public IActionResult AddVehicle([FromBody]  VehicleModel vehicleModel)
        {
            try
            {
                if (vehicleModel.VehicleType >= 0 && vehicleModel.VehicleType <= 3)
                {
                    Vehicle vehicle = new Vehicle(vehicleModel.Id, (VehicleType)vehicleModel.VehicleType, vehicleModel.Balance);
                    _parkingService.AddVehicle(vehicle);

                    return Created($"api/vehicles/{vehicle.Id}", new JsonResult(vehicle).Value);
                }
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    
        [HttpDelete("{id}")]
        public IActionResult RemoveVehicle(string id)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (!Vehicle.Validation(id)) return BadRequest();
            if (vehicle == null) return NotFound();
            else
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
        }
    }
}