﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using System.IO;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet("last")]
        public IActionResult GetLastTransactions()
        {
            return new JsonResult(_parkingService.GetLastParkingTransactions().ToArray());
        }
        [HttpGet("all")]
        public IActionResult GetAllTransactions()
        {
            try
            {
                var result = _parkingService.ReadFromLog();
                return new JsonResult(result);
            }
            catch (FileNotFoundException) { return NotFound(); }
        }
        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody] TopUpModel topUp)
        {
            //var item = JsonConvert.DeserializeObject<TopUpModel>(topUp.ToString());
            if (!Vehicle.Validation(topUp.Id)||topUp.Sum<0) { return BadRequest(); }
            
            try
            {
                _parkingService.TopUpVehicle(topUp.Id, topUp.Sum);
                var result = _parkingService.GetVehicles().Single(x => x.Id == topUp.Id);
                return new JsonResult(result);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}