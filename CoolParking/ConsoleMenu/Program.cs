﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.WebAPI;
using System.Text;
using System.Linq.Expressions;

namespace ConsoleMenu
{
    class Program
    {

        static async Task Main()
        {
            //Thread thread = new Thread(new ThreadStart(Working));
            //thread.Start();

            Console.WriteLine("Привiт! Вiтаю тебе на CoolParking!");
            Vehicle vehicle = new Vehicle(VehicleType.Motorcycle, 40);
            Console.WriteLine(vehicle.ToString());
            var json = JsonConvert.SerializeObject(vehicle.ToString());
            Console.WriteLine(json);
            Console.WriteLine(JsonConvert.DeserializeObject(json));
            bool flag = true;
            int answ = 0;
            while (flag == true)
            {
                await ChooseAction();
                Console.WriteLine("Чи хочеш ти виконати ще якусь дiю? 1-так/2-нi");
                answ = int.Parse(Console.ReadLine());
                if (answ != 1)
                    flag = false;
            }
            Console.WriteLine("На все добре!");

        }
        private const string APP_PATH = "https://localhost:44303/api/";
        static readonly HttpClient client = new HttpClient();
        static HttpResponseMessage response { get; set; }
        static string responseBody { get; set; }
        private static async Task Get(string token)
        {
            response = await client.GetAsync(APP_PATH + token);
            response.EnsureSuccessStatusCode();
            responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        private static async Task Post(string token)
        {
            Vehicle vehicle = CreateVehicle();
            var content = new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.UTF8, "application/json");
            response = await client.PostAsync(APP_PATH + token, content);
            response.EnsureSuccessStatusCode();
            responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        private static async Task Put()
        {
            TopUpModel model =CreateTopUpModel();
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            response = await client.PutAsync(APP_PATH + $"transactions/{model.Id}", content);
            response.EnsureSuccessStatusCode();
            responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        private static async Task Delete(string token)
        {
            Console.WriteLine("Введіть ідентифікатор засобу, який ви хочете видалити");
            string tok = Console.ReadLine();
            token += tok;
            response = await client.DeleteAsync(APP_PATH + token);
            response.EnsureSuccessStatusCode();
            responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        public static async Task ChooseAction()
        {
            int action = 0;
            bool flag = false;
            Console.WriteLine("Обери, що ти хочеш зробити");
            Console.WriteLine("0-Отримати транспорт за  Id\n1-Побачити поточний баланс CoolParking \n2-Побачити загальну місткість паркінгу\n" +
             "3-Побачити кiлькість вiльних мiсць\n4-Побачити список транспортих засобiв, що зараз знаходяться на паркiнгу" +
             "\n5-Додати новий транспортний засiб\n6-Забрати транспортний засiб з паркiнгу\n7-Поповнити баланс конкретного транспортного засобу\n" +
             "8-Побачити Транзакцiї за поточний перiод\n9-Отримати останнi транзакцiї");
            Console.WriteLine("Введи свою відповiдь: ");
            try
            {
                action = int.Parse(Console.ReadLine());
                flag = true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                action = int.Parse(Console.ReadLine()); flag = true;
            }

            while (flag == true)
            {
                switch (action)
                {
                    case 1:
                        await Get("parking/balance");
                        flag = false;
                        break;
                    case 2:
                        await Get("parking/capacity");
                        flag = false;
                        break;
                    case 3:
                        await Get("parking/freePlaces");
                        flag = false;
                        break;
                    case 4:
                        await Get("vehicles");
                        flag = false;
                        break;
                    case 5:
                        await Post("vehicles");
                        flag = false;
                        break;
                    case 6:
                        await Delete("vehicles/");
                        flag = false;
                        break;
                    case 7:
                        await Put();
                        flag = false;
                        break;
                    case 8:
                        await Get("all");
                        flag = false;
                        break;
                    case 9:
                        await Get("transactions/last");
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("Введи один з варіантів");
                        break;
                }
            }
        }
       public static TopUpModel CreateTopUpModel()
        {
            Console.WriteLine("Input id");
            string id = Console.ReadLine();
            Console.WriteLine("Input sum");
            decimal sum = decimal.Parse(Console.ReadLine());
            return new TopUpModel { Id=id,Sum=sum};
        }
        public static Vehicle CreateVehicle()
        {
            VehicleType type = VehicleType.Bus;
            decimal balance = 0;
            string ID;
            bool repeat = false;
            int input;
            Vehicle vehicle = null;
            Console.WriteLine("Ти обрав додати транспортний засiб на парковку!");
            Thread.Sleep(5);
            Console.WriteLine("Обери тип транспортного засобу");
            Console.WriteLine("1-Автобус, 2-Легковий автомобiль,\n 3-Грузовий автомобiль, 4-Мотоцикл");
            input = int.Parse(Console.ReadLine());
            while (repeat == false)
            {
                switch (input)
                {
                    case 1:
                        type = VehicleType.Bus;
                        repeat = true;
                        break;
                    case 2:
                        type = VehicleType.PassengerCar;
                        repeat = true;
                        break;
                    case 3:
                        type = VehicleType.Truck;
                        repeat = true;
                        break;
                    case 4:
                        type = VehicleType.Motorcycle;
                        repeat = true;
                        break;
                    default:
                        Console.WriteLine("Введи коректне значення");
                        break;
                }
            }
            Console.WriteLine("Додай суму для свого балансу");
            try
            {
                balance = decimal.Parse(Console.ReadLine());
            }
            catch (Exception) { new Exception("Введи коректне значення"); }
            Console.WriteLine("Додати iдентифiкатор транспортного засобу можна двома способами:\n1-Введу самостiйно, 2-Згенерувати автоматично");
            input = int.Parse(Console.ReadLine());
            repeat = true;
            while (repeat == true)
            {
                switch (input)
                {
                    case 1:
                        Console.WriteLine("Окей, тепер введи iдентифiктор в такому форматi: XX-YYYY-XX, де Х-будь-яка латинська буква у верхньому регiстрi, Y-будь-яка цифра");
                        ID = Console.ReadLine();
                        vehicle = new Vehicle(ID, type, balance);
                        repeat = false;
                        break;
                    case 2:
                        vehicle = new Vehicle(type, balance);
                        Console.WriteLine("Окей, твiй ідентифiкатор: {0}", vehicle.Id);
                        repeat = false;
                        break;
                    default:
                        Console.WriteLine("Обери коректний варіант");
                        break;

                }
            }
            //parkingService.AddVehicle(vehicle);
            return vehicle;
        }
    }
}
