﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Design;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Linq;
using System.Timers;
using System.Transactions;
using System.Reflection;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace CoolParking.BL.Services
{
    public class ParkingService:IParkingService
    {
        readonly Parking parking;
        ILogService LogServ { get; set; }
        ITimerService TimerPay { get; set; }
        ITimerService TimerLog { get; set; }
        public ParkingService(ITimerService timer5,ITimerService timer60, ILogService logService)
        {
            TimerLog = timer60;
            TimerPay = timer5;
            LogServ = logService;
            TimerLog.Elapsed += WriteToLog;
            TimerPay.Elapsed += MakeTransaction;
            TimerPay.Interval = Settings.PayTime;
            TimerLog.Interval = Settings.WritePeriod;
            parking = Parking.GetInstance();
            TimerLog.Start();
            TimerPay.Start();
        }
        public decimal GetBalance()
        {
            return parking.Balance;
        }
        public int GetCapacity()
        {
            return parking.vehicleList.Capacity;
        }
        public int GetFreePlaces()
        {
            return parking.vehicleList.Capacity - parking.vehicleList.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.vehicleList);
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (!UniqId(vehicle.Id)) 
             throw new ArgumentException("Транспортний засіб з таки ідентифікатором вже існує на парковці");
            if (parking.vehicleList.Count == 10) throw new InvalidOperationException();
            else parking.vehicleList.Add(vehicle); 
        }
        public bool UniqId(string id)
        {
            return !parking.vehicleList.Select(v => v.Id).Contains(id);
        }
        public void RemoveVehicle(string vehicleId)
        {
            var item = parking.vehicleList.FirstOrDefault(v => v.Id==vehicleId);
            if (item==null)
            {
                throw new ArgumentException("На парковці не існує такого транспортного засобу");
            }
            if (item.Balance < 0) throw new InvalidOperationException();
            else parking.vehicleList.Remove(item);
        }
        public void TopUpVehicle(string vehicleId,decimal sum)
        {
            Vehicle item = (from i in parking.vehicleList where i.Id == vehicleId select i).FirstOrDefault();
            if (item==null) throw new ArgumentException();
            if (sum < 0) throw new ArgumentException();
            else item.Balance += sum;
            //parking.TransactionList.Add(new TransactionInfo(vehicleId,sum));
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return parking.TransactionList.ToArray();
        }
        public void MakeTransaction(object obj,ElapsedEventArgs args)
        {
            foreach (Vehicle vehicle in parking.vehicleList)
            {
                decimal paySum = 0;
                switch (vehicle.VehicleType)
                {
                    case VehicleType.Bus:
                        paySum = vehicle.Balance - Settings.BusPay > 0 ?
                           Settings.BusPay : -vehicle.Balance + Settings.BusPay < Settings.TruckPay ?
                           (Settings.BusPay - vehicle.Balance) * Settings.Fine + vehicle.Balance : Settings.BusPay * Settings.Fine;
                        break;
                    case VehicleType.Motorcycle:
                        paySum = vehicle.Balance - Settings.MotorcyclePay > 0 ?
                           Settings.MotorcyclePay : -vehicle.Balance + Settings.MotorcyclePay < Settings.MotorcyclePay ?
                           (Settings.MotorcyclePay - vehicle.Balance) * Settings.Fine + vehicle.Balance : Settings.MotorcyclePay * Settings.Fine;
                        break;
                    case VehicleType.PassengerCar:
                        paySum = vehicle.Balance - Settings.CarPay > 0 ?
                           Settings.CarPay : -vehicle.Balance + Settings.CarPay < Settings.CarPay ?
                           (Settings.CarPay - vehicle.Balance) * Settings.Fine + vehicle.Balance : Settings.CarPay * Settings.Fine;
                        break;
                    case VehicleType.Truck:
                        paySum = vehicle.Balance - Settings.TruckPay > 0 ?
                           Settings.TruckPay : -vehicle.Balance+ Settings.TruckPay < Settings.TruckPay ?
                           (Settings.TruckPay - vehicle.Balance) * Settings.Fine + vehicle.Balance:Settings.TruckPay*Settings.Fine;
                        break;
                }
                vehicle.Balance -= paySum;
                parking.Balance += paySum;
                parking.TransactionList.Add(new TransactionInfo(vehicle.Id, paySum));
            }
        }        
        public virtual void WriteToLog(object obj,ElapsedEventArgs args)
        {
            string info = WriteAndReset();
            LogServ.Write(info);
        }
        public string ReadFromLog()
        {
            return LogServ.Read();
        }
        public void Dispose()
        {
            parking.vehicleList = new List<Vehicle>(Settings.ParkingSize);
            parking.Balance = Settings.StartBalance;
            parking.TransactionList = new List<TransactionInfo>();
        }
        public string WriteAndReset()
        {
            string write = "";

            foreach (var item in parking.TransactionList)
            {
                write += $"Час транзакції: {item.Time}; Сума транзакції:\t{item.Sum};\tІдентифікатор транспортного засобу{item.VehicleId}\n";
            }
            parking.TransactionList.RemoveRange(0, parking.TransactionList.Count());
            return write;
        }

    }
}