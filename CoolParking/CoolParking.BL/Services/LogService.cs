﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;
using CoolParking.BL.Models; 

namespace CoolParking.BL.Services
{
    public class LogService:ILogService
    {
        public string LogPath { get;  }
        public TransactionInfo Transaction { get; }
        public LogService(string logpath)
        {
            LogPath = logpath;
        }
        public void Write(string loginfo)
        {
            using StreamWriter streamWriter = new StreamWriter(LogPath, true);
            streamWriter.WriteLine(loginfo);

        }
        public string Read()
        {
            string fileContent;
            try
            {
                fileContent = File.ReadAllText(LogPath);
            }
            catch (FileNotFoundException)
            {
                throw new InvalidOperationException();
            }
            return fileContent;
        }
    }
}
