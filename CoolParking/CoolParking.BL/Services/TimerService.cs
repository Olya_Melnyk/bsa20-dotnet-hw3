﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;
//using System.Threading;
namespace CoolParking.BL.Services
{
    public class TimerService:ITimerService
    {
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        private static Timer aTimer;
        public TimerService()
        {
            aTimer = new Timer();
        }
       public TimerService(double interval)
        {
            aTimer = new Timer();
            Interval = interval;
        }
        public void Start()
        {
            aTimer.Interval = Interval;
            aTimer.Elapsed += Elapsed;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
            aTimer.Start();
        }
        public void Stop()
        {
            aTimer.Elapsed -= Elapsed;
            aTimer.Stop();
        }
        public void Dispose()
        {
            aTimer.Dispose();
        }
    }
}