﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
using System;
using System.Runtime.Serialization;

namespace CoolParking.BL.Models
{
   public enum VehicleType
    {
        PassengerCar,
        Bus,
        Truck,
        Motorcycle
    }

}
