﻿using System;
//using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using Newtonsoft.Json;
namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get;  }
        public decimal Balance { get; set; }
        public VehicleType VehicleType { get;  }
        [JsonConstructor]
        public Vehicle(string id,VehicleType type, decimal balance)
        {
                Id = Validation(id) ? id : throw new ArgumentException("Не коректний ідентифікатор транспортного засобу");
                VehicleType = VehicleType;
                Balance = balance>=0?balance:throw new ArgumentException("Баланс не може бути від'ємним при додаванні транспортного засобу");
        }
       // public Vehicle() { Id = GenerateRandomRegistrationPlateNumber();VehicleType = 0;Balance = 0; }
        public Vehicle(VehicleType type, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = type;
            Balance = balance >= 0 ? balance : throw new ArgumentException("Баланс не може бути від'ємним при додаванні транспортного засобу");
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rdn = new Random();
            string st = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return (st[rdn.Next(0, st.Length - 1)]).ToString() + (st[rdn.Next(0, st.Length - 1)]).ToString()
                + "-" + rdn.Next(1000, 9999) + "-" + (st[rdn.Next(0, st.Length - 1)]).ToString() + (st[rdn.Next(0, st.Length - 1)]).ToString();
        }
        public static bool Validation(string id)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            return regex.IsMatch(id);
        }
        public override string ToString()
        {
            return  Id.ToString() +
                "vehicleType:" + VehicleType.ToString()+
               "balance:" + Balance.ToString();
        }
    }
}